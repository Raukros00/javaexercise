public class Main extends Persona{

    private String lavoro;

    public Main(String nome, String cognome, String lavoro){
        super(nome, cognome);
        setLavoro(lavoro);
    }

    public void setLavoro(String lavoro) { this.lavoro = lavoro; }
    public String getLavoro() { return lavoro; }

    public static void main(String[] args) {
        Main m = new Main("Alessandro", "Dominici", "Ingegnere");
        System.out.println("Ciao " + m.getNome() + " il tuo lavoro è " + m.getLavoro());
        m.setNome("Mario");
        m.setCognome("Rossi");
        System.out.println("Ciao " + m.getNome() + " " + m.getCognome() + " il tuo lavoro è " + m.getLavoro());
    }
}